import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {sha512} from 'js-sha512';
import {jwtDecode} from 'jwt-decode';
import {BookshelfJwtPayload} from '../../model/jwtToken';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  login(username:string, password:string) {
    const body = {
      username,
      password: sha512(password+username.toUpperCase())
    };
    return this.httpClient.post('login', body, {responseType: 'text'})
      .pipe(tap(res => localStorage.setItem('access_token', res)));
  }

  isLogged() {
    const token = this.getToken();
    if(token === null){
      return false
    }
    const decodedToken = jwtDecode(token);
    if(decodedToken.exp < Math.floor(Date.now() / 1000)){
      this.logout();
      return false;
    }
    return true;
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  getJwtToken() {
    return jwtDecode<BookshelfJwtPayload>(this.getToken());
  }

  logout() {
    localStorage.removeItem('access_token');
  }
}
