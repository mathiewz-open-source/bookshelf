import {Injectable, Renderer2} from '@angular/core';
import {Themes} from '../config/themes';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  renderer: Renderer2

  loadTheme() {
    this.setTheme(this.getCurrentTheme());
  }

  getCurrentTheme(){
    return localStorage.getItem('theme') ?? (this.isOsDarkMode() ? Themes.darkTheme : Themes.lightTheme);
  }

  setTheme(themeName: string) {
    localStorage.setItem('theme', themeName);
    this.renderer.setAttribute(document.querySelector('html'), 'data-theme', themeName);
  }

  private isOsDarkMode() {
    return window.matchMedia('(prefers-color-scheme: dark)')?.matches ?? false;
  }
}
