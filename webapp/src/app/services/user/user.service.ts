import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import { HttpClient } from "@angular/common/http";
import {User} from "../../model/user";
import {map} from "rxjs/operators";
import {Auteur} from "../../model/auteur";
import {InvitForm} from "../../model/invitForm";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getUser() : Observable<User>{
    return this.httpClient.get<User>('user');
  }

  getUsers() : Observable<User[]>{
    return this.httpClient.get<User[]>('users').pipe(map(users => users.map(user => new User(user))));
  }

  getFavoriteAuthor() : Observable<Auteur>{
    return this.httpClient.get<Auteur>('user/favorites/author').pipe(map(auteur => new Auteur(auteur)));
  }

  getFavoriteEdition() : Observable<string>{
    return this.httpClient.get('user/favorites/edition', {responseType: 'text'});
  }

  newUser(form: InvitForm) : Observable<number>{
    return this.httpClient.post<number>('user/invite', form)
  }
}
