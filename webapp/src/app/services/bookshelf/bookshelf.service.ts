import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Bookshelf} from "../../model/bookshelf";
import { HttpClient } from "@angular/common/http";
import {map} from "rxjs/operators";
import {Livre} from "../../model/livre";

@Injectable({
  providedIn: 'root'
})
export class BookshelfService {

  constructor(private httpClient: HttpClient) { }

  getBookshelf() : Observable<Bookshelf>{
    return this.httpClient.get<Bookshelf>('bookshelf').pipe(
      map((bookshelf: Bookshelf) => new Bookshelf(bookshelf))
    );
  }

  batchUpdate(livres: Livre[]) : Observable<Bookshelf>{
    return this.httpClient.put<Bookshelf>('bookshelf', livres).pipe(
      map((bookshelf: Bookshelf) => new Bookshelf(bookshelf))
    );
  }
}
