import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import { HttpClient, HttpParams } from "@angular/common/http";
import {Livre} from "../../model/livre";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(
    private httpClient: HttpClient
  ) { }

  registerBook(livre: Livre) : Observable<Livre>{
    return this.httpClient.post<Livre>('books', livre).pipe(
      map(livre => new Livre(livre))
    );
  }

  editBook(id: string, livre: Livre) : Observable<Livre>{
    return this.httpClient.put<Livre>('books/'+id, livre).pipe(
      map(livre => new Livre(livre))
    );
  }

  getBook(id: string) : Observable<Livre>{
    return this.httpClient.get<Livre>('books/'+id).pipe(
      map(livre => new Livre(livre))
    );
  }

  deleteBook(id: string) : Observable<any> {
    return this.httpClient.delete<Livre>('books/'+id);
  }

  setRead(id: string, lu: boolean) {
    return this.httpClient.post<Livre>('books/'+id+'/read', {'lu': lu}).pipe(
      map(livre => new Livre(livre))
    );
  }

  addToUserBookshelf(id: string) : Observable<Livre>{
    return this.httpClient.put<Livre>('books/'+id+'/bookshelf', {}).pipe(
      map(livre => new Livre(livre))
    );
  }

  removeFromUserBookshelf(id: string) : Observable<Livre>{
    return this.httpClient.delete<Livre>('books/'+id+'/bookshelf', {}).pipe(
      map(livre => new Livre(livre))
    );
  }


  search(term: string) : Observable<Livre[]>{
    if(term.length == 0){
      return of([]);
    }
    let params = new HttpParams().set('criteria', term);
    return this.httpClient.get<Livre[]>('books', {params: params}).pipe(
      map(livres => livres.map(livre => new Livre(livre)))
    );
  }

  getBookForUser() : Observable<Livre[]>{
    return this.httpClient.get<Livre[]>('user/books').pipe(
      map(livres => livres.map(livre => new Livre(livre)))
    );
  }

  importFromISBN(isbn: string) : Observable<Livre> {
    return this.httpClient.get<Livre>('books/isbn/'+isbn).pipe(
      map(livre => new Livre(livre))
    );
  }
}
