export class Auteur {
  id: number;
  nom: string;
  prenom: string;

  constructor(data: any) {
    if(data == null){
      return;
    }
    this.id = data.id;
    this.nom = data.nom;
    this.prenom = data.prenom;
  }

  getDisplayName(){
    if(this.prenom){
      return this.prenom + ' ' + this.nom;
    } else {
      return this.nom;
    }
  }
}
