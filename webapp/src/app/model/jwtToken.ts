import {JwtPayload} from 'jwt-decode';

export interface BookshelfJwtPayload extends JwtPayload {
  id: number;
  username: string;
}
