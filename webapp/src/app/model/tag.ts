export class Tag {

  id: number;
  label: string;
  icon: string;
  color: string;

  constructor(data: any) {
    if (data == null) {
      return;
    }
    this.id = data.id;
    this.label = data.label;
    this.icon = data.icon;
    this.color = data.color;
  }
}
