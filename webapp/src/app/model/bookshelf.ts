import {Livre} from "./livre";

export class Bookshelf {
  nom: string;
  nbLivres: number;
  totalPrice: number;
  livres: Livre[];

  constructor(data: any) {
    if(data == null){
      return;
    }
    this.nom = data.nom;
    this.nbLivres = data.nbLivres;
    this.totalPrice = data.totalPrice;
    this.livres = data.livres.map(livre => new Livre(livre));

  }
}
