import {Component, OnInit} from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {AppComponent} from '../../../app.component';
import {BookService} from '../../../services/book/book.service';
import {Livre} from '../../../model/livre';
import {faEdit, faMinusSquare, faPlusSquare, faTrash} from '@fortawesome/free-solid-svg-icons';
import {UserService} from '../../../services/user/user.service';
import {User} from '../../../model/user';

@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.scss']
})
export class BookViewComponent implements OnInit {

  livre: Livre;
  currentUser: User;

  faPlus = faPlusSquare;
  faMinus= faMinusSquare;
  faEdit = faEdit;
  faTrash = faTrash;

  constructor(
    private appComponent: AppComponent,
    private route: ActivatedRoute,
    private bookService: BookService,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.appComponent.setTitle('Livre');
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.bookService.getBook(params.get('id')))
    ).subscribe(
      res => {
        this.livre = res;
        this.appComponent.setTitle(this.livre.titre);
      },
      error => {
        console.error(error);
        void this.router.navigateByUrl('/404');
      }
    );
    this.userService.getUser().subscribe(
      res => this.currentUser = res,
      err => console.error(err)
    )
  }

  read(value: boolean){
    this.bookService.setRead(this.livre.id, value).subscribe(
      res => this.livre = res,
      err => console.error(err)
    );
  }

  delete() {
    this.bookService.deleteBook(this.livre.id).subscribe(
      () => this.router.navigateByUrl('/'),
      err => console.error(err)
    );
  }

  addToBookshelf() {
    this.bookService.addToUserBookshelf(this.livre.id).subscribe(
      res => this.livre = res,
      err => console.error(err)
    )
  }

  removeFromBookshelf() {
    this.bookService.removeFromUserBookshelf(this.livre.id).subscribe(
      res => this.livre = res,
      err => console.error(err)
    )
  }
}
