import {Component, OnInit} from '@angular/core';
import {LibelleService} from '../../../services/libelle.service';
import {AppComponent} from '../../../app.component';
import {UserService} from '../../../services/user/user.service';
import {Livre} from '../../../model/livre';
import {BookService} from '../../../services/book/book.service';
import {Router} from '@angular/router';
import {faSearch, faHourglass} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-ajout-livre',
  templateUrl: './ajout-livre.component.html',
  styleUrls: ['./ajout-livre.component.scss']
})
export class AjoutLivreComponent implements OnInit {

  pageTitle: string;
  isbn: string;
  livre: Livre;
  addOnCreate: boolean;
  faHourglass = faHourglass;
  faSearch = faSearch;
  panel: number;
  searching: boolean;
  autoLivre: Livre;

  constructor(
    private libelleService: LibelleService,
    private userService: UserService,
    private bookService: BookService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.libelleService.getLibelle('addBook.title').subscribe(
      res => this.pageTitle = res,
      error => console.error(error)
    );
    this.appComponent.setTitle('Nouveau livre');
    this.livre = new Livre({});
    this.addOnCreate = false;
    this.panel = 0;
    this.searching = false;
  }

  newBook(livre: Livre) {
    this.bookService.registerBook(livre).subscribe(
      res => {
        if(this.addOnCreate){
          this.bookService.addToUserBookshelf(res.id).subscribe(
            res => this.router.navigateByUrl('/book/' + res.id),
            error => console.error(error)
          );
        } else {
          this.router.navigateByUrl('/book/' + res.id);
        }
      },error => console.error(error)
    )
  }

  searchISBN() {
    console.log('looking for ISBN : '+this.isbn);
    this.searching = true;
    this.autoLivre= null;
    this.bookService.importFromISBN(this.isbn).subscribe(
      res => {
        this.livre = res;
        this.autoLivre = res;
        this.panel = 1;
      },
      err => console.log(err)
    ).add(() => this.searching = false);
  }
}
