import { Component, OnInit } from '@angular/core';
import {BookService} from '../../../services/book/book.service';
import {Livre} from '../../../model/livre';
import {Auteur} from '../../../model/auteur';
import {UserService} from '../../../services/user/user.service';
import {AppComponent} from '../../../app.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  public livres: Livre[];
  public favoriteAuthor: Auteur;
  public favoriteEdition: string;
  public livresFromAuthor: Livre[];
  public livresFromEdition: Livre[];

  public doughnutChartOptions: any = {
    legend: {
      position: 'right'
    },
    responsive: true
  }

  constructor(
    private bookService: BookService,
    private userService: UserService,
    private appComponent: AppComponent
  ) { }

  ngOnInit(): void {
    this.appComponent.setTitle('Tableau de bord')
    this.bookService.getBookForUser().subscribe(
      res => {
        this.livres = res;
        this.userService.getFavoriteAuthor().subscribe(
          favoriteAuthor => {
            this.favoriteAuthor = favoriteAuthor;
            this.livresFromAuthor = this.getLivresFromAuthor();
          },
          error => console.error(error)
        );
        this.userService.getFavoriteEdition().subscribe(
          favoriteEdition => {
            this.favoriteEdition = favoriteEdition;
            this.livresFromEdition = this.getLivresFromEdition();
          },
          error => console.error(error)
        );
      },
      err => console.error(err)
    );
  }

  public getNbLivres(): number{
    return this.livres?.length ?? 0;
  }

  public getNbLivresLus(): number{
    return this.livres?.filter(livre => livre.lu).length ?? 0;
  }

  public getNbLivresPretes(): number{
    return this.livres?.filter(livre => livre.pret).length ?? 0;
  }

  public getTotalPrice(): number{
    return this.livres?.map(livre => livre.prix).reduce((a, b) => a + b, 0) ?? 0;
  }

  public getLivresFromAuthor(): Livre[]{
    if(!this.favoriteAuthor){
      return [];
    }
    return this.livres.filter(livre => livre.auteur.id === this.favoriteAuthor.id);
  }

  public getLivresFromEdition(): Livre[]{
    if(!this.favoriteEdition){
      return [];
    }
    return this.livres.filter(livre => livre.edition === this.favoriteEdition);
  }

}
