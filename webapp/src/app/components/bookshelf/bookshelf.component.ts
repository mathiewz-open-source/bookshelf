import {Component, inject, OnInit, ViewChild} from '@angular/core';
import {Bookshelf} from '../../model/bookshelf';
import {BookshelfService} from '../../services/bookshelf/bookshelf.service';
import {Livre} from '../../model/livre';
import {AuthService} from '../../services/auth/auth.service';
import {AppComponent} from '../../app.component';
import {faFilter} from '@fortawesome/free-solid-svg-icons';
import {BooklistComponent} from './booklist/booklist.component';

interface FilterInterface {
  name: string;
  selected: boolean;
  filter(l: Livre): boolean;
}

@Component({
  selector: 'app-bookshelf',
  templateUrl: './bookshelf.component.html',
  styleUrls: ['./bookshelf.component.scss']
})
export class BookshelfComponent implements OnInit {

  private bookshelfService = inject(BookshelfService);
  private authService = inject(AuthService);
  private appComponent = inject(AppComponent);

  @ViewChild('booklist') booklist: BooklistComponent;

  filters: FilterInterface[] = [
    {
      name: 'Seulement mes livres',
      filter: l => l.detenteurs.some(u => u.userId === this.authService.getJwtToken().id),
      selected: false
    },
    {
      name: 'Seulement les livres lus',
      filter: l => l.lu,
      selected: false
    },
    {
      name: 'Seulement les livres non lus',
      filter: l => !l.lu,
      selected: false
    }
  ]

  bookshelf: Bookshelf;
  filteredBooks: Livre[];

  logged: boolean;

  updating: boolean;

  protected readonly faFilter = faFilter;

  ngOnInit() {
    this.bookshelfService.getBookshelf().subscribe(
      result => {
        this.bookshelf = result;
        this.resetFilters();
        this.appComponent.setBaseTitle(result.nom);
      },
      error => console.error(error)
    );
    this.appComponent.setTitle();
    this.updating = false;
    this.logged = this.authService.isLogged();
  }

  markListAsLu(lu: boolean) {
    const livresDto : Livre[] = this.booklist.checkedBooks.map(id => new Livre({id, lu}))
    this.updating = true;
    this.bookshelfService.batchUpdate(livresDto).subscribe(
      result => {
        this.bookshelf = result;
        this.booklist.checkedBooks = [];
        this.resetFilters();
      },
      error => console.error(error)
    ).add(() => this.updating = false)
  }

  toogleFilter(filter: FilterInterface) {
    filter.selected = !filter.selected;
    this.updateFilters();
  }

  private resetFilters() {
    this.filters.forEach(f => f.selected = false);
    this.updateFilters();
  }

  private updateFilters(){
    this.filteredBooks = this.bookshelf.livres;
    this.filters.filter(f => f.selected)
      .forEach(f => this.filteredBooks = this.filteredBooks.filter(f.filter))
  }
}
