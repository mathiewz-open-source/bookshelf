import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  environment: {production: boolean};
  authFailure: boolean;

  constructor(
    private authService : AuthService
  ) {
  }

  ngOnInit() {
    this.authFailure = false;
    this.environment = environment
  }

  login(form: any) {
    this.authService.login(form.username, form.password).subscribe(
      () => location.reload(),
      error => {
        this.authFailure = true;
        console.error(error);
      }
    );
  }
}
