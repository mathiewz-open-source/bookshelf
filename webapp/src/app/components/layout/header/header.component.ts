import {Component, ElementRef, HostListener, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';
import {UserService} from '../../../services/user/user.service';
import {User} from '../../../model/user';
import {faBook, faSearch, faPlusSquare, faUser} from '@fortawesome/free-solid-svg-icons';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {ThemeToggleComponent} from './theme-toggle/theme-toggle.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('dropdown') dropdown: ElementRef;
  @ViewChildren(ThemeToggleComponent) themeToggle: QueryList<ThemeToggleComponent>;

  logged: boolean;
  user: User;
  faBooks= faBook;
  faSearch= faSearch;
  faPlusSquare= faPlusSquare;
  faUser= faUser;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.logged = this.authService.isLogged();
    if(this.logged){
      this.userService.getUser().subscribe(
        user => this.user = user,
        error => console.error(error)
      );
    }

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((_: NavigationEnd) => {
        this.closeList();
    });
  }

  logout() {
    this.authService.logout();
    location.reload();
  }

  @HostListener('window:click', ['$event.target'])
  onClick(target) {
    if(!this.dropdown?.nativeElement.contains(target)){
      this.closeList();
    }
  }

  closeList() {
    this.dropdown?.nativeElement.removeAttribute('open');
  }

  toggleTheme() {
    this.themeToggle.forEach(t => t.toggleCheck())
  }
}
