import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from '../../app.component';
import {InvitForm} from '../../model/invitForm';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {UserService} from '../../services/user/user.service';
import {sha512} from 'js-sha512';
import {AuthService} from '../../services/auth/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {

  code: string;
  pageTitle: string;

  form: FormGroup;
  private invitForm: InvitForm;
  errorMessage: string;


  constructor(
    private route: ActivatedRoute,
    private appComponent: AppComponent,
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.invitForm = new InvitForm();
    this.buildForm();
    this.setPageTitle('Lien d\'invitation')
    this.route.paramMap.subscribe( paramMap => {
      this.invitForm.code = paramMap.get('code');
    })
  }

  private buildForm() {
    const checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
      const pass = group.get('password').value;
      const confirmPass = group.get('confirmPassword').value
      if (pass === confirmPass) {
        return null;
      } else {
        group.get('confirmPassword').setErrors({notSame: true});
        return {notSame: true};
      }
    }

    this.form = new FormGroup({
      identifiant: new FormControl(this.invitForm.identifiant, [
        Validators.required
      ]),
      password: new FormControl(this.invitForm.password, [
        Validators.required
      ]),
      confirmPassword: new FormControl(undefined),
      displayName: new FormControl(this.invitForm.displayName, [
        Validators.required
      ]),
      bibliothequeName: new FormControl(this.invitForm.bibliothequeName, [
        Validators.required
      ])
    }, {validators :checkPasswords})
  }

  setPageTitle(title: string){
    this.pageTitle = title;
    this.appComponent.setTitle(this.pageTitle);
  }

  sendForm() {
    this.invitForm.identifiant = this.form.value.identifiant;
    this.invitForm.password = sha512(this.form.value.password+this.form.value.identifiant.toUpperCase());
    this.invitForm.bibliothequeName = this.form.value.bibliothequeName;
    this.invitForm.displayName = this.form.value.displayName;


    this.userService.newUser(this.invitForm).subscribe(
      () => this.login(),
      err => {
        console.error(err);
        this.errorMessage = err.error;
      }
    )
  }

  private login() : Subscription{
    return this.authService.login(this.form.value.identifiant, this.form.value.password).subscribe(
      () => this.router.navigateByUrl('/').then(() => location.reload()),
      err => console.log(err)
    );
  }
}
