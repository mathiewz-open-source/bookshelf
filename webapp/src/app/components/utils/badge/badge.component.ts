import {Component, Input, OnInit} from '@angular/core';
import {IconDefinition} from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent implements OnInit {

  @Input() color : string;
  @Input() icon: IconDefinition | string;

  constructor() { }

  ngOnInit(): void {
  }

  getIcon() : IconDefinition {
    if(typeof this.icon === 'string'){
      return fas[this.icon];
    }
    return this.icon;
  }

}
