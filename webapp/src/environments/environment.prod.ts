export const environment = {
  production: true,
  apiUrl: 'https://api.bookshelf-project.com',
  gaTrackingId: 'G-6WWK1THWRV'
};
