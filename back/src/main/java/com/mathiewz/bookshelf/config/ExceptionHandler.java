package com.mathiewz.bookshelf.config;

import com.mathiewz.bookshelf.exception.NotFoundException;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    @ServerExceptionMapper({NotFoundException.class})
    public RestResponse<String> handleNotFound(Exception e) {
        LOGGER.warn("Erreur 404", e);
        return RestResponse.status(RestResponse.Status.NOT_FOUND, e.getMessage());
    }

}
