package com.mathiewz.bookshelf.config.security;


import com.mathiewz.bookshelf.exception.BadJwtSignatureException;
import io.smallrye.jwt.auth.principal.*;
import io.smallrye.jwt.util.KeyUtils;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.lang.JoseException;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@ApplicationScoped
@Alternative
@Priority(1)
public class JwtPrincipalFactory extends JWTCallerPrincipalFactory {

    private final SecretKey key;

    public JwtPrincipalFactory(
            @ConfigProperty(name = "security.secret") String secret
    ) {
        key = KeyUtils.createSecretKeyFromSecret(secret);
    }

    @Override
    public JWTCallerPrincipal parse(String token, JWTAuthContextInfo authContextInfo) throws ParseException {
        try {
            JsonWebSignature jws = new JsonWebSignature();
            jws.setKey(key);
            jws.setCompactSerialization(token);
            if (!jws.verifySignature()) {
                throw new BadJwtSignatureException("bad jwt signature :(");
            }
            // Token has already been verified, parse the token claims only
            String json = new String(Base64.getUrlDecoder().decode(token.split("\\.")[1]), StandardCharsets.UTF_8);
            return new DefaultJWTCallerPrincipal(JwtClaims.parse(json));
        } catch (InvalidJwtException | JoseException ex) {
            throw new ParseException(ex.getMessage());
        }
    }


}