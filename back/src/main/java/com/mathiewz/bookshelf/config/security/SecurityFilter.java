package com.mathiewz.bookshelf.config.security;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.ext.Provider;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.ClaimValue;

@Provider
@PreMatching
@RequestScoped
public class SecurityFilter implements ContainerRequestFilter {

    @Inject
    AuthenticationContext authCtx;

    @Inject
    @Claim("id")
    ClaimValue<Long> userId;

    public void filter(ContainerRequestContext requestContext) {
        authCtx.setCurrentUserId(userId.getValue());
    }
}