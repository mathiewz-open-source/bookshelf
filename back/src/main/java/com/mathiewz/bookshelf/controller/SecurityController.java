package com.mathiewz.bookshelf.controller;

import com.mathiewz.bookshelf.entity.AuthenticationRequest;
import com.mathiewz.bookshelf.entity.User;
import com.mathiewz.bookshelf.service.SecurityService;
import jakarta.annotation.security.PermitAll;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import org.jboss.resteasy.reactive.RestResponse;

@Path("/login")
@PermitAll
public class SecurityController {

    private final SecurityService securityService;

    public SecurityController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @POST
    public RestResponse<String> authenticate(AuthenticationRequest authenticationRequest) {
        User user = securityService.authenticate(authenticationRequest);
        if (user == null) {
            return RestResponse.ResponseBuilder.create(RestResponse.Status.UNAUTHORIZED, "Authentication failed").build();
        }
        return RestResponse.ResponseBuilder.ok(securityService.createToken(user)).build();
    }

}
