package com.mathiewz.bookshelf.controller;

import com.mathiewz.bookshelf.dto.TagDTO;
import com.mathiewz.bookshelf.service.ParameterService;
import jakarta.annotation.security.PermitAll;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import java.util.List;

@Path("/params")
@PermitAll
public class ParameterController {

    private final ParameterService parameterService;

    public ParameterController(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    @GET
    @Path("/tags")
    public List<TagDTO> getTags(){
        return parameterService.getTags();
    }

}
