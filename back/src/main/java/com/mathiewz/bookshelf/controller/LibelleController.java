package com.mathiewz.bookshelf.controller;

import com.mathiewz.bookshelf.service.LibelleService;
import jakarta.annotation.security.PermitAll;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

@Path("/libelle")
@PermitAll
public class LibelleController {

    private final LibelleService libelleService;

    public LibelleController(LibelleService libelleService) {
        this.libelleService = libelleService;
    }

    @GET
    public String getLibelle(@QueryParam("name") String libelleName){
        return libelleService.findLibelle(libelleName);
    }

}
