package com.mathiewz.bookshelf.controller;

import com.mathiewz.bookshelf.dto.AuteurDTO;
import com.mathiewz.bookshelf.dto.InvitationDTO;
import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.dto.UserDTO;
import com.mathiewz.bookshelf.exception.InvalidCodeException;
import com.mathiewz.bookshelf.exception.UserAlreadyExistsException;
import com.mathiewz.bookshelf.service.*;
import io.quarkus.security.Authenticated;
import jakarta.annotation.security.PermitAll;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import java.math.BigInteger;
import java.util.List;

@Path("/user")
@Authenticated
public class UserController {

    private final UserService userService;
    private final BookshelfService bookshelfService;
    private final EditionService editionService;
    private final AuteurService auteurService;
    private final SecurityService securityService;

    public UserController(
            UserService userService,
            BookshelfService bookshelfService,
            EditionService editionService,
            AuteurService auteurService,
            SecurityService securityService
    ) {
        this.userService = userService;
        this.bookshelfService = bookshelfService;
        this.editionService = editionService;
        this.auteurService = auteurService;
        this.securityService = securityService;
    }

    @GET
    public UserDTO getUser(){
        return new UserDTO(securityService.getCurrentUser());
    }

    @GET
    @Path("/books")
    public List<LivreDTO> getUserBooks(){
        return bookshelfService.getLivresForCurrentUser();
    }

    @GET
    @Path("/favorites/author")
    public AuteurDTO getAuteurFavoris(){
        return auteurService.getCurrentUserFavorite();
    }

    @GET
    @Path("/favorites/edition")
    public String getEditionFavorite(){
        return editionService.getCurrentUserFavorite();
    }

    @POST
    @PermitAll
    @Path("/invite")
    public BigInteger newUser(@RequestBody InvitationDTO invitationDTO) throws InvalidCodeException, UserAlreadyExistsException {
        return userService.newUser(invitationDTO);
    }

}
