package com.mathiewz.bookshelf.controller;

import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.dto.LivreFormDTO;
import com.mathiewz.bookshelf.service.BookService;
import io.quarkus.security.Authenticated;
import jakarta.ws.rs.*;

import java.util.Set;

@Path("/books")
@Authenticated
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GET
    public Set<LivreDTO> findBooks(@QueryParam("criteria") String criteria){
        return bookService.searchBook(criteria);
    }

    @GET
    @Path("/{id}")
    public LivreDTO getBook(@PathParam("id") String id){
        return bookService.getLivre(id);
    }

    @PUT
    @Path("/{id}")
    public LivreDTO updateBook(@PathParam("id") String id, LivreFormDTO livre){
        return bookService.updateLivre(id, livre);
    }

    @DELETE
    @Path("/{id}")
    public void deleteBook(@PathParam("id") String id){
        bookService.deleteLivre(id);
    }

    @POST
    public LivreDTO newBook(LivreFormDTO livre){
        return bookService.registerBook(livre);
    }

    public record SetReadBody(boolean lu) {}

    @POST
    @Path("/{id}/read")
    public LivreDTO setRead(
            @PathParam("id") String id,
            SetReadBody userMap
    ){
        return bookService.readBook(id, userMap.lu);
    }

    @PUT
    @Path("/{id}/bookshelf")
    public LivreDTO addToBookshelf(@PathParam("id") String id){
        return bookService.addBook(id);
    }

    @DELETE
    @Path("/{id}/bookshelf")
    public LivreDTO removeFromBookshelf(@PathParam("id") String id){
        return bookService.removeBook(id);
    }

    @GET
    @Path("isbn/{isbn}")
    public LivreDTO importByISBN(@PathParam("isbn") String isbn){
        return bookService.findByIsbn(isbn);
    }
}
