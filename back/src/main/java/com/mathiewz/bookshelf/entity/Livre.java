package com.mathiewz.bookshelf.entity;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "livres")
public class Livre {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "auteur_id")
    private Auteur auteur;

    @Column(name = "titre")
    private String titre;

    @ManyToOne
    @JoinColumn(name = "edition_id")
    private Edition edition;

    @Column(name = "prix")
    private float prix;

    @OneToMany(mappedBy = "livre", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    private Set<LivrePossede> detenteurs;

    @Column(name = "pret")
    private boolean pret;

    @OneToMany(mappedBy = "livre", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<StatutLecture> lecteurs;

    @ManyToMany
    @JoinTable(name = "book_tags",
            joinColumns = { @JoinColumn(name = "livre") },
            inverseJoinColumns = { @JoinColumn(name = "tag") }
    )
    private Set<Tag> tags;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Auteur getAuteur() {
        return auteur;
    }

    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Edition getEdition() {
        return edition;
    }

    public void setEdition(Edition edition) {
        this.edition = edition;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public Set<LivrePossede> getDetenteurs() {
        if(detenteurs == null){
            detenteurs = new HashSet<>();
        }
        return detenteurs;
    }

    public void setDetenteurs(Set<LivrePossede> detenteurs) {
        this.detenteurs = detenteurs;
    }

    public boolean isPret() {
        return pret;
    }

    public void setPret(boolean pret) {
        this.pret = pret;
    }

    public Set<StatutLecture> getLecteurs() {
        if(lecteurs == null){
            lecteurs = new HashSet<>();
        }
        return lecteurs;
    }

    public void setLecteurs(Set<StatutLecture> lecteurs) {
        this.lecteurs = lecteurs;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

}
