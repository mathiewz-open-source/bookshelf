package com.mathiewz.bookshelf.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "livre_statut")
public class StatutLecture {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "livre")
    private Livre livre;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "statut")
    private String statut;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Livre getLivre() {
        return livre;
    }

    public void setLivre(Livre livre) {
        this.livre = livre;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
}
