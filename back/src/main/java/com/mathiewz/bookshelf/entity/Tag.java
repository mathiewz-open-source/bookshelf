package com.mathiewz.bookshelf.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "tags")
public class Tag {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "label")
    private String label;
    @Column(name = "icon")
    private String icon;
    @Column(name = "icon_code_point")
    private Integer iconCode;
    @Column(name = "color")
    private String color;

    public long getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getIcon() {
        return icon;
    }

    public Integer getIconCode() {
        return iconCode;
    }

    public String getColor() {
        return color;
    }
}
