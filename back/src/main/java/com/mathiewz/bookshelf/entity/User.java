package com.mathiewz.bookshelf.entity;

import jakarta.persistence.*;

import java.math.BigInteger;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "display_name")
    private String displayName;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "bibliotheque")
    private Bibliotheque bibliotheque;

    @OneToMany(mappedBy = "user")
    private Set<StatutLecture> statutslivres;

    @OneToMany(mappedBy = "user")
    private Set<LivrePossede> livresPossedes;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Bibliotheque getBibliotheque() {
        return bibliotheque;
    }

    public void setBibliotheque(Bibliotheque bibliotheque) {
        this.bibliotheque = bibliotheque;
    }

    public Set<StatutLecture> getStatutslivres() {
        return statutslivres;
    }

    public void setStatutslivres(Set<StatutLecture> statutslivres) {
        this.statutslivres = statutslivres;
    }

    public Set<LivrePossede> getLivresPossedes() {
        return livresPossedes;
    }

    public void setLivresPossedes(Set<LivrePossede> livresPossedes) {
        this.livresPossedes = livresPossedes;
    }
}
