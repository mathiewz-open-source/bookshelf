package com.mathiewz.bookshelf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "libelles")
public class Libelle {

    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "libelle")
    private String value;

    public String getValue() {
        return value;
    }
}
