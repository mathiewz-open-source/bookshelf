package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Auteur;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class AuteurDAO implements PanacheRepository<Auteur> {

    public Optional<Auteur> findByNomAndPrenom(String nom, String prenom){
        var params = Parameters
                .with("nom", nom)
                .and("prenom", prenom);
        return find("nom = :nom and prenom = :prenom", params).firstResultOptional();
    }

}
