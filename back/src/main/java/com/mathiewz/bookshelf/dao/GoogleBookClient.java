package com.mathiewz.bookshelf.dao;


import com.mathiewz.bookshelf.dto.google.SearchResults;
import io.quarkus.rest.client.reactive.ClientQueryParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(baseUri = "https://www.googleapis.com/books/")
@ClientQueryParam(name = "key", value = "${api.google.key}")
public interface GoogleBookClient {

    @GET
    @Path("/v1/volumes")
    SearchResults findByISBN(@QueryParam("q") String isbn);

}
