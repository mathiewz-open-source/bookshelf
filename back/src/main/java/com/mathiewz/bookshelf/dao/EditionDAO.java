package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Edition;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class EditionDAO implements PanacheRepository<Edition> {

    public Optional<Edition> findByNom(String nom){
        return find("nom", nom).firstResultOptional();
    }

}
