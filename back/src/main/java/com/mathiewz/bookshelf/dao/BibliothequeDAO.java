package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Bibliotheque;
import com.mathiewz.bookshelf.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.stream.Stream;

@ApplicationScoped
public class BibliothequeDAO implements PanacheRepository<Bibliotheque> {

    public Stream<Bibliotheque> findAllByUsersContains(User user){
        return find("user in :user", user).stream();
    }

}
