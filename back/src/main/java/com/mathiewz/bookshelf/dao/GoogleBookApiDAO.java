package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Livre;
import jakarta.enterprise.context.ApplicationScoped;
import com.mathiewz.bookshelf.mapper.LivreMapper;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@ApplicationScoped
public class GoogleBookApiDAO {
    private final GoogleBookClient webClient;
    private final LivreMapper livreMapper;


    public GoogleBookApiDAO(
            @RestClient GoogleBookClient webClient,
            LivreMapper livreMapper
    ) {
        this.livreMapper = livreMapper;
        this.webClient = webClient;
    }

    public Livre findByISBN(String isbn){
        return livreMapper.buildLivre(webClient.findByISBN("isbn:" + isbn));
    }

}
