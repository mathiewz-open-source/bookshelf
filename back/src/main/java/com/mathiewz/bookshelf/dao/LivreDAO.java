package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Livre;
import com.mathiewz.bookshelf.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class LivreDAO implements PanacheRepository<Livre> {

    public List<Livre> findAllByUser(User user){
        return list("FROM Livre l " +
                "LEFT JOIN FETCH l.edition " +
                "LEFT JOIN FETCH l.detenteurs " +
                "LEFT JOIN FETCH l.lecteurs " +
                "LEFT JOIN FETCH l.tags " +
                "LEFT JOIN FETCH l.auteur " +
                "JOIN l.detenteurs owner " +
                "WHERE owner.user = ?1", user);
    }

    public List<Livre> searchBookByName(String criteria){
        return list("lower(titre) like lower(concat('%', ?1,'%'))", criteria);
    }

    public List<Livre> searchBookByAuthor(String criteria){
        return list("lower(auteur.nom) like lower(concat('%', ?1,'%')) or lower(auteur.prenom) like lower(concat('%', ?1,'%'))", criteria);
    }

}
