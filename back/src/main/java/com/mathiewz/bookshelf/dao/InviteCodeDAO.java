package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.InviteCode;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class InviteCodeDAO implements PanacheRepository<InviteCode> {

    public Optional<InviteCode> findByCodeAndUsedFalse(String code){
        return find("code = :code and used = false", code).firstResultOptional();
    }

}
