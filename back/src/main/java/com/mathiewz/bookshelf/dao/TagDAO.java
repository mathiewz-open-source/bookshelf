package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Tag;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TagDAO implements PanacheRepository<Tag> {

}
