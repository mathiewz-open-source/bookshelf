package com.mathiewz.bookshelf.mapper;

import com.mathiewz.bookshelf.dao.TagDAO;
import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.dto.LivreFormDTO;
import com.mathiewz.bookshelf.dto.TagDTO;
import com.mathiewz.bookshelf.dto.UserDTO;
import com.mathiewz.bookshelf.dto.google.BookItem;
import com.mathiewz.bookshelf.dto.google.SaleInfo;
import com.mathiewz.bookshelf.dto.google.SearchResults;
import com.mathiewz.bookshelf.dto.google.VolumeInfo;
import com.mathiewz.bookshelf.entity.*;
import com.mathiewz.bookshelf.exception.NotFoundException;
import com.mathiewz.bookshelf.service.AuteurService;
import com.mathiewz.bookshelf.service.EditionService;
import com.mathiewz.bookshelf.service.SecurityService;
import com.mathiewz.bookshelf.service.UserService;
import jakarta.enterprise.context.ApplicationScoped;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class LivreMapper {

    private final SecurityService securityService;
    private final AuteurService auteurService;
    private final EditionService editionService;
    private final UserService userService;
    private final TagDAO tagDAO;

    public LivreMapper(SecurityService securityService, AuteurService auteurService, EditionService editionService, UserService userService, TagDAO tagDAO) {
        this.securityService = securityService;
        this.auteurService = auteurService;
        this.editionService = editionService;
        this.userService = userService;
        this.tagDAO = tagDAO;
    }

    public LivreDTO buildDTO(Livre livre){
        LivreDTO dto = new LivreDTO(livre);
        User currentUser = securityService.getCurrentUser();
        if(currentUser != null){
            dto.setPrix(livre.getPrix());
            if(livre.getLecteurs().stream().anyMatch(lecteur -> Objects.equals(lecteur.getUser().getId(), currentUser.getId()))){
                dto.setLu(true);
            }
        }
        return dto;
    }

    public void dtoToEntity(LivreFormDTO livreDto, Livre livre){
        livre.setTitre(livreDto.getTitre());
        livre.setPret(livreDto.isPret());
        livre.setPrix(livreDto.getPrix());
        livre.setAuteur(auteurService.getOrCreateAuteur(livreDto.getAuteur()));
        livre.setEdition(editionService.getOrCreateEdition(livreDto.getEdition()));

        Set<BigInteger> detenteursFromDB = livre.getDetenteurs().stream()
                .map(LivrePossede::getUser)
                .map(User::getId)
                .collect(Collectors.toSet());
        Set<BigInteger> detenteursFromDto = livreDto.getDetenteurs().stream()
                .map(UserDTO::getUserId)
                .collect(Collectors.toSet());

        detenteursFromDto.stream()
                .filter(userID -> !detenteursFromDB.contains(userID))
                .map(userID -> {
                    LivrePossede livrePossede = new LivrePossede();
                    livrePossede.setLivre(livre);
                    livrePossede.setUser(userService.getUser(userID.longValue()));
                    livrePossede.setDateAjout(LocalDate.now());
                    return livrePossede;
                }).forEach(lp -> livre.getDetenteurs().add(lp));
        livre.getDetenteurs().stream()
                .filter(lp -> !detenteursFromDto.contains(lp.getUser().getId()))
                .forEach(lp -> livre.getDetenteurs().remove(lp));

        Set<Tag> tags = livreDto.getTags().stream()
                .map(TagDTO::getId)
                .map(tagDAO::findByIdOptional)
                .flatMap(Optional::stream)
                .collect(Collectors.toSet());
        livre.setTags(tags);
    }

    public Livre buildLivre(SearchResults searchResults){
        if(searchResults.getTotalItems() == 0){
            throw new NotFoundException("Aucun résultat correspondant à l'ISBN fourni");
        }
        if(searchResults.getTotalItems() > 1){
            throw new IllegalArgumentException("Too many results");
        }
        Livre result = new Livre();
        BookItem bookItem = searchResults.getItems().get(0);
        VolumeInfo volumeInfo = bookItem.getVolumeInfo();

        Edition edition = new Edition();
        edition.setNom(volumeInfo.getPublisher());
        result.setEdition(edition);

        Auteur auteur = new Auteur();
        String[] names = volumeInfo.getAuthors().get(0).split(" ", 2);
        if(names.length >= 2){
            auteur.setPrenom(names[0]);
        }
        auteur.setNom(names[names.length-1]);
        result.setAuteur(auteur);

        SaleInfo saleInfo = bookItem.getSaleInfo();
        if(saleInfo.getRetailPrice() != null && saleInfo.getRetailPrice().getCurrencyCode().equals("EUR")){
            result.setPrix(saleInfo.getRetailPrice().getAmount());
        }

        result.setTitre(volumeInfo.getTitle());
        result.setTags(Set.of());

        return result;
    }
}
