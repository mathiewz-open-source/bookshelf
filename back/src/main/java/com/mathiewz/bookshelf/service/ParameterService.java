package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.dao.TagDAO;
import com.mathiewz.bookshelf.dto.TagDTO;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class ParameterService {

    private final TagDAO tagDAO;

    public ParameterService(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public List<TagDTO> getTags(){
        return tagDAO.findAll().stream()
                .map(TagDTO::new)
                .toList();
    }

}
