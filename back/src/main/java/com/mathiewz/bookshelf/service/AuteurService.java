package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.dao.AuteurDAO;
import com.mathiewz.bookshelf.dto.AuteurDTO;
import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.entity.Auteur;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@ApplicationScoped
public class AuteurService {

    private final BookshelfService bookshelfService;
    private final AuteurDAO auteurDAO;

    public AuteurService(
            BookshelfService bookshelfService,
            AuteurDAO auteurDAO
    ) {
        this.bookshelfService = bookshelfService;
        this.auteurDAO = auteurDAO;
    }

    public Auteur getOrCreateAuteur(AuteurDTO auteur){
        return auteurDAO.findByNomAndPrenom(auteur.getNom(), auteur.getPrenom()).orElseGet(() -> registerAuteur(auteur));
    }

    private Auteur registerAuteur(AuteurDTO dto) {
        Auteur auteur = new Auteur();
        auteur.setNom(dto.getNom());
        auteur.setPrenom(dto.getPrenom());
        auteurDAO.persist(auteur);
        return auteur;
    }

    public AuteurDTO getCurrentUserFavorite() {
        Map<AuteurDTO, Long> authorCount =  bookshelfService.getLivresForCurrentUser().stream()
                .collect(Collectors.groupingBy(LivreDTO::getAuteur, Collectors.counting()));

        return Collections.max(authorCount.entrySet(), Map.Entry.comparingByValue()).getKey();
    }
}
