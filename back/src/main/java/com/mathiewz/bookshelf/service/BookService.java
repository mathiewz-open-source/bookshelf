package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.dao.GoogleBookApiDAO;
import com.mathiewz.bookshelf.dao.LivreDAO;
import com.mathiewz.bookshelf.dao.LivrePossedeDAO;
import com.mathiewz.bookshelf.dao.StatutLectureDAO;
import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.dto.LivreFormDTO;
import com.mathiewz.bookshelf.entity.Livre;
import com.mathiewz.bookshelf.entity.LivrePossede;
import com.mathiewz.bookshelf.entity.StatutLecture;
import com.mathiewz.bookshelf.entity.User;
import com.mathiewz.bookshelf.exception.NotFoundException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import com.mathiewz.bookshelf.mapper.LivreMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookService.class);

    private final LivreDAO livreDAO;
    private final LivrePossedeDAO livrePossedeDAO;
    private final SecurityService securityService;
    private final LivreMapper livreMapper;
    private final StatutLectureDAO statutLectureDAO;
    private final GoogleBookApiDAO googleBookApiDAO;

    public BookService(
            LivreDAO livreDAO,
            LivrePossedeDAO livrePossedeDAO,
            SecurityService securityService,
            LivreMapper livreMapper,
            StatutLectureDAO statutLectureDAO,
            GoogleBookApiDAO googleBookApiDAO
    ) {
        this.livreDAO = livreDAO;
        this.livrePossedeDAO = livrePossedeDAO;
        this.securityService = securityService;
        this.livreMapper = livreMapper;
        this.statutLectureDAO = statutLectureDAO;
        this.googleBookApiDAO = googleBookApiDAO;
    }

    public LivreDTO getLivre(String id){
        return livreDAO.findByIdOptional(Long.valueOf(id))
                .map(livreMapper::buildDTO)
                .orElseThrow(() -> new NotFoundException("Aucun livre avec l'ID "+id));
    }

    public Set<LivreDTO> searchBook(String criteria){
        List<Livre> searchResults = livreDAO.searchBookByName(criteria);
        searchResults.addAll(livreDAO.searchBookByAuthor(criteria));
        return searchResults.stream()
                .map(livreMapper::buildDTO)
                .collect(Collectors.toSet());
    }

    @Transactional
    public LivreDTO registerBook(LivreFormDTO livreDto) {
        Livre livre = new Livre();
        livreMapper.dtoToEntity(livreDto, livre);
        livre.setLecteurs(new HashSet<>());
        livreDAO.persist(livre);
        return livreMapper.buildDTO(livre);
    }

    @Transactional
    public LivreDTO updateLivre(String id, LivreFormDTO livreDto) {
        Livre livre = getLivreEntity(id);
        livreMapper.dtoToEntity(livreDto, livre);
        livreDAO.persist(livre);
        return livreMapper.buildDTO(livre);
    }

    @Transactional
    public void deleteLivre(String id) {
        Livre livre = getLivreEntity(id);
        LOGGER.info("Suppression du livre {} par {}", livre.getTitre(), securityService.getCurrentUser().getDisplayName());
        livreDAO.deleteById(Long.valueOf(id));
    }

    @Transactional
    public LivreDTO readBook(String id, boolean lu) {
        Livre livre = getLivreEntity(id);
        User currentUser = securityService.getCurrentUser();
        if(lu){
            StatutLecture statut = new StatutLecture();
            statut.setStatut("LU");
            statut.setLivre(livre);
            statut.setUser(currentUser);
            livre.getLecteurs().add(statut);
            statutLectureDAO.persist(statut);
        } else {
            Optional<StatutLecture> statut = statutLectureDAO.findByLivreAndUser(livre, currentUser);
            statut.ifPresent(statutLectureDAO::delete);
            statut.map(StatutLecture::getLivre)
                    .map(Livre::getLecteurs)
                    .ifPresent(lecteurs -> lecteurs.remove(statut.get()));
        }
        return livreMapper.buildDTO(livre);
    }

    @Transactional
    public LivreDTO addBook(String bookId) {
        Livre livre = getLivreEntity(bookId);
        User currentUser = securityService.getCurrentUser();
        LivrePossede livrePossede = new LivrePossede();
        livrePossede.setLivre(livre);
        livrePossede.setUser(currentUser);
        livrePossede.setDateAjout(LocalDate.now());
        livre.getDetenteurs().add(livrePossede);
        livrePossedeDAO.persist(livrePossede);
        return livreMapper.buildDTO(livre);
    }

    @Transactional
    public LivreDTO removeBook(String bookId) {
        Livre livre = getLivreEntity(bookId);
        User currentUser = securityService.getCurrentUser();
        livrePossedeDAO.findByLivreAndUser(livre, currentUser).ifPresent(livrePossedeDAO::delete);
        livre.getDetenteurs().removeIf(owner -> owner.getUser().equals(currentUser));
        return livreMapper.buildDTO(livre);
    }

    public LivreDTO findByIsbn(String isbn) {
        String cleanIsbn = isbn
                .replaceAll("-", "")
                .replaceAll(" ", "");
        Livre livre = googleBookApiDAO.findByISBN(cleanIsbn);
        return livreMapper.buildDTO(livre);
    }

    private Livre getLivreEntity(String id) {
        return livreDAO.findByIdOptional(Long.valueOf(id))
                .orElseThrow(() -> new NotFoundException("Aucun livre avec l'ID "+ id));
    }

}
