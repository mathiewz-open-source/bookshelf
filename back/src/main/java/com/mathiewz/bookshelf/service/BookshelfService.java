package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.dao.LivreDAO;
import com.mathiewz.bookshelf.dto.BookshelfDTO;
import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.entity.Bibliotheque;
import com.mathiewz.bookshelf.entity.Livre;
import com.mathiewz.bookshelf.entity.User;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import com.mathiewz.bookshelf.mapper.LivreMapper;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class BookshelfService {

    private static final Comparator<Livre> BY_AUTHOR_NAME = Comparator.comparing(livre -> livre.getAuteur().getNom());

    private final LivreMapper livreMapper;
    private final LivreDAO livreDAO;
    private final SecurityService securityService;

    public BookshelfService(
            LivreMapper livreMapper,
            LivreDAO livreDAO,
            SecurityService securityService
    ) {
        this.livreMapper = livreMapper;
        this.livreDAO = livreDAO;
        this.securityService = securityService;
    }

    @Transactional
    public BookshelfDTO getAllLivres(){
        User currentUser = securityService.getCurrentUser();
        Bibliotheque bibliotheque = currentUser.getBibliotheque();

        List<LivreDTO> livres = bibliotheque.getUsers().stream()
                .map(livreDAO::findAllByUser)
                .flatMap(List::stream)
                .distinct()
                .sorted(BY_AUTHOR_NAME)
                .map(livreMapper::buildDTO)
                .collect(Collectors.toList());

        return new BookshelfDTO(bibliotheque.getNom(), livres);
    }

    public List<LivreDTO> getLivresForCurrentUser() {
        User currentUser = securityService.getCurrentUser();
        return livreDAO.findAllByUser(currentUser).stream()
                .sorted(BY_AUTHOR_NAME)
                .map(livreMapper::buildDTO)
                .collect(Collectors.toList());
    }
}
