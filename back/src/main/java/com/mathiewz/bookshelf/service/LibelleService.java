package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.dao.LibelleDAO;
import com.mathiewz.bookshelf.exception.NotFoundException;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class LibelleService {

    private final LibelleDAO libelleDao;

    public LibelleService(LibelleDAO libelleDao) {
        this.libelleDao = libelleDao;
    }

    public String findLibelle(String libelleName) {
        return libelleDao.findForName(libelleName).orElseThrow(() -> new NotFoundException("Aucun libelle avec la clé "+libelleName));
    }
}
