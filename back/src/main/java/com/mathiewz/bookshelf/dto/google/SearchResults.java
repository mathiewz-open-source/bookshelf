package com.mathiewz.bookshelf.dto.google;

import java.util.List;

public class SearchResults {

    private int totalItems;
    private List<BookItem> items;

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public List<BookItem> getItems() {
        return items;
    }

    public void setItems(List<BookItem> items) {
        this.items = items;
    }
}
