String? requiredField(value) {
  return value == null || value.isEmpty ? 'Champ requis' : null;
}