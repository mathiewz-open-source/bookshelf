import 'package:bookshelf/ThemeSettings.dart';
import 'package:bookshelf/pages/login.dart';
import 'package:bookshelf/services/auth_service.dart';
import 'package:bookshelf/widget/card_padding.dart';
import 'package:flutter/material.dart';

class DebugPage extends StatefulWidget {
  const DebugPage({super.key});

  @override
  State<DebugPage> createState() => DebugPageState();
}

class DebugPageState extends State<DebugPage> {
  final authService = AuthService();
  final themeSettings = ThemeSettings();

  @override
  Widget build(BuildContext context) {
    var themeModeSwitch = ListenableBuilder(
        listenable: themeSettings,
        builder: (BuildContext context, Widget? child) =>
            SegmentedButton<ThemeMode>(
              segments: const [
                ButtonSegment(
                    value: ThemeMode.light,
                    label: Text('Light'),
                    icon: Icon(Icons.light_mode)),
                ButtonSegment(
                    value: ThemeMode.system,
                    label: Text('System'),
                    icon: Icon(Icons.smartphone)),
                ButtonSegment(
                    value: ThemeMode.dark,
                    label: Text('Dark'),
                    icon: Icon(Icons.dark_mode)),
              ],
              selected: {themeSettings.themeMode},
              onSelectionChanged: (value) =>
                  themeSettings.setThemeMode(value.first),
            ));

    var logoutButton = FilledButton(
      onPressed: () => logout().then((_) => Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const LoginForm()))),
      child: const Text("Déconnexion"),
    );

    colorEntry(color, label) => DropdownMenuEntry<Color>(
        leadingIcon: Icon(Icons.palette_outlined, color: color),
        value: color,
        label: label);

    var themePicker = DropdownMenu<Color>(
        label: const Text("Theme"),
        leadingIcon: const Icon(Icons.palette),
        onSelected: (color) {
          if (color != null) {
            themeSettings.changeThemeColor(color);
          }
        },
        dropdownMenuEntries: [
          colorEntry(Colors.deepPurple, "Deep purple"),
          colorEntry(Colors.indigo, "Indigo"),
          colorEntry(Colors.blue, "Blue"),
          colorEntry(Colors.teal, "Teal"),
          colorEntry(Colors.green, "Green"),
          colorEntry(Colors.yellow, "Yellow"),
          colorEntry(Colors.amber, "Amber"),
          colorEntry(Colors.orange, "Orange"),
          colorEntry(Colors.deepOrange, "Deep orange"),
          colorEntry(Colors.pink, "Pink"),
        ]);

    return Scaffold(
        appBar: AppBar(
          title: const Text("Debug tools"),
        ),
        body: Container(
          alignment: Alignment.center,
          child: Column(children: [
            CardPadding(
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Column(children: [themePicker, const Divider(), themeModeSwitch])
            ])),
            CardPadding(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [logoutButton])),
          ]),
        ));
  }

  logout() async {
    await authService.logout();
  }
}
