import 'package:bookshelf/models/livre.dart';
import 'package:bookshelf/services/user_service.dart';
import 'package:flutter/material.dart';
import 'package:bookshelf/ThemeSettings.dart';
import 'package:bookshelf/pages/login.dart';
import 'package:bookshelf/services/auth_service.dart';
import 'package:bookshelf/widget/card_padding.dart';
import 'package:fl_chart/fl_chart.dart';

class ComptePage extends StatefulWidget {
  const ComptePage({super.key});

  @override
  State<ComptePage> createState() => ComptePageState();
}

class ComptePageState extends State<ComptePage> {
  final authService = AuthService();
  final themeSettings = ThemeSettings();
  final userService = UserService();

  late Future<List<Livre>> _futureBooks;
  bool loading = true;

  set futureBooks(Future<List<Livre>> futureBooks) {
    setState(() {
      _futureBooks = futureBooks.whenComplete(() => setState(() {
            loading = false;
          }));
    });
  }

  @override
  void initState() {
    super.initState();
    futureBooks = userService.getUserBooks();
  }

  @override
  Widget build(BuildContext context) {
    var logoutButton = FilledButton(
      onPressed: () => logout().then((_) => Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const LoginForm()))),
      child: const Text("Déconnexion"),
    );

    return Scaffold(
        appBar: AppBar(
          title: const Text("Mon compte"),
        ),
        body: Container(
          alignment: Alignment.center,
          child:
              SingleChildScrollView(child :Column(children: [dashboardCard(), readCard(), themeCard(), CardPadding(child: logoutButton)])),
        ));
  }

  Widget dashboardCard() {

    buildLoaded(BuildContext context, List<Livre> livres) {
      double prixTotal = livres.map((l) => l.prix).reduce((a, b) => a+b);
      int nbLu = livres.where((l) => l.lu).length;
      int nbPrets = livres.where((l) => l.pret).length;

      String dashboardText = '''Vous avez dans votre bibliothèque ${livres.length} livres.

Cela vous à couté ${prixTotal.toStringAsFixed(2)}€.

Sur ${livres.length} livres, vous en avez lu $nbLu.

Vous avez prêté $nbPrets livres, ne les oubliez pas !''';

      return CardPadding(title: "En bref", child: Text(dashboardText));
    }

    return FutureBuilder(
      future: _futureBooks,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return buildLoaded(context, snapshot.data!);
        }
        return Container();
      },
    );
  }

  Widget readCard(){
    buildLoaded(BuildContext context, List<Livre> livres) {
      return CardPadding(title: "Voyons ça", child: AspectRatio(aspectRatio: 1.5, child: PieChart(
        PieChartData(
            sections: [
              PieChartSectionData(value: livres.where((l) => l.lu).length.toDouble(), title: "Lu", radius: 120, color: Theme.of(context).colorScheme.secondaryContainer),
              PieChartSectionData(value: livres.where((l) => !l.lu).length.toDouble(), title: "Non-Lu", radius: 120, color: Theme.of(context).colorScheme.onSecondary),
            ],
            centerSpaceRadius: 0,
        ),
      )));
    }

    return FutureBuilder(
      future: _futureBooks,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return buildLoaded(context, snapshot.data!);
        }
        return Container();
      },
    );
  }

  Widget themeCard() {
    var themeModeSwitch = ListenableBuilder(
        listenable: themeSettings,
        builder: (BuildContext context, Widget? child) =>
            SegmentedButton<ThemeMode>(
              segments: const [
                ButtonSegment(
                    value: ThemeMode.light,
                    label: Text('Clair'),
                    icon: Icon(Icons.light_mode)),
                ButtonSegment(
                    value: ThemeMode.system,
                    label: Text('Système'),
                    icon: Icon(Icons.smartphone)),
                ButtonSegment(
                    value: ThemeMode.dark,
                    label: Text('Sombre'),
                    icon: Icon(Icons.dark_mode)),
              ],
              selected: {themeSettings.themeMode},
              onSelectionChanged: (value) =>
                  themeSettings.setThemeMode(value.first),
            ));

    colorEntry(color, label) => DropdownMenuEntry<Color>(
        leadingIcon: Icon(Icons.palette_outlined, color: color),
        value: color,
        label: label);

    var themePicker = ListenableBuilder(
        listenable: themeSettings,
        builder: (BuildContext context, Widget? child) => DropdownMenu<Color>(
                label: const Text("Couleur"),
                leadingIcon:
                    Icon(Icons.palette, color: themeSettings.seedColor),
                initialSelection: themeSettings.seedColor,
                onSelected: (color) {
                  if (color != null) {
                    themeSettings.changeThemeColor(color);
                  }
                },
                dropdownMenuEntries: [
                  colorEntry(Colors.deepPurple, "Deep purple"),
                  colorEntry(Colors.indigo, "Indigo"),
                  colorEntry(Colors.blue, "Bleu"),
                  colorEntry(Colors.teal, "Turquoise"),
                  colorEntry(Colors.green, "Vert"),
                  colorEntry(Colors.yellow, "Jaune"),
                  colorEntry(Colors.amber, "Ambre"),
                  colorEntry(Colors.orange, "Orange"),
                  colorEntry(Colors.deepOrange, "Deep orange"),
                  colorEntry(Colors.pink, "Rose"),
                ]));

    var themeCard = CardPadding(
      title: "Thème",
      child: Column(children: [
        Padding(padding: const EdgeInsets.only(bottom: 8), child: themePicker),
        themeModeSwitch
      ]),
    );
    return themeCard;
  }

  logout() async {
    await authService.logout();
  }
}
