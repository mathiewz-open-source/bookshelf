import 'package:bookshelf/pages/bibliotheque.dart';
import 'package:bookshelf/pages/compte.dart';
import 'package:bookshelf/pages/debug.dart';
import 'package:bookshelf/pages/search.dart';
import 'package:bookshelf/services/auth_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TabNavigation extends StatefulWidget {
  const TabNavigation({super.key});

  @override
  State<TabNavigation> createState() => _TabNavigationState();
}

class _TabNavigationState extends State<TabNavigation> {
  int currentPageIndex = 0;

  final authService = AuthService();

  @override
  Widget build(BuildContext context) {

    var destinations = <Widget>[
      const NavigationDestination(
        icon: Icon(Icons.shelves),
        label: 'Bibliothèque',
      ),
      const NavigationDestination(
        icon: Icon(Icons.search),
        label: 'Rechercher',
      ),
      const NavigationDestination(
        selectedIcon: Icon(Icons.account_circle),
        icon: Icon(Icons.account_circle_outlined),
        label: 'Mon compte',
      ),
    ];

    var pages = <Widget>[
      const BibliothequePage(),
      const SearchPage(),
      const ComptePage(),
    ];

    if (kDebugMode) {
      destinations.add(const NavigationDestination(
        selectedIcon: Icon(Icons.bug_report),
        icon: Icon(Icons.bug_report_outlined),
        label: 'Debug',
      ));
      pages.add(const DebugPage());
    }

    return Scaffold(
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
            currentPageIndex = index;
          });
        },
        selectedIndex: currentPageIndex,
        destinations: destinations,
      ),
      body: pages[currentPageIndex],
    );
  }
}
