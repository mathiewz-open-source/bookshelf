import 'package:bookshelf/http/backend_http_client.dart';
import 'package:bookshelf/models/tag.dart';

class ParamsService{

  Future<List<Tag>> getTags() async {
    var response = await backend.get('params/tags');
    var rawList = response.data as List;
    return rawList.map((json) => Tag.fromJson(json)).toList();
  }

}