import 'dart:convert';

import 'package:bookshelf/models/isbn_search.dart';
import 'package:bookshelf/models/livre.dart';

import '../http/backend_http_client.dart';

class LivreService {

  Future<Livre> add(Livre livre) async {
    var response = await backend.post('books', data: jsonEncode(livre.toJson()));
    return Livre.fromJson(response.data);
  }

  Future<Livre> edit(Livre livre) async {
    var response = await backend.put('books/${livre.id}', data: jsonEncode(livre.toJson()));
    return Livre.fromJson(response.data);
  }

  Future<IsbnSearch> findByISBN(String isbn) async {
    var response = await backend.get('books/isbn/$isbn');
    return IsbnSearch.fromJson(response.data);
  }

  Future<Livre> fetchLivre(String livreId) async {
    var response = await backend.get('books/$livreId');
    return Livre.fromJson(response.data);
  }

  Future<Livre> setRead(String livreId, bool lu) async {
    var body = jsonEncode({'lu': lu});

    var response = await backend.post('books/$livreId/read', data: body);
    return Livre.fromJson(response.data);
  }

  Future<Livre> addToUserBookshelf(String id) async {
    var response = await backend.put('books/$id/bookshelf');
    return Livre.fromJson(response.data);
  }

  Future<Livre> removeFromUserBookshelf(String id) async {
    var response = await backend.delete('books/$id/bookshelf');
    return Livre.fromJson(response.data);
  }

  Future<void> delete(String id) async {
    await backend.delete('books/$id');
  }

  Future<List<Livre>> search(String term) async {
    if (term.isEmpty) {
      return [];
    }

    var response = await backend.get('books', queryParameters: {'criteria': term});
    var rawList = response.data as List;
    return rawList.map((json) => Livre.fromJson(json)).toList();
  }


}
