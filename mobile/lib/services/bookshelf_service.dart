import 'package:bookshelf/http/backend_http_client.dart';
import 'package:bookshelf/models/bibliotheque.dart';

class BibliothequeService {

  Future<Bibliotheque> fetchBibliotheque() async {
    var response = await backend.get('bookshelf');
    return Bibliotheque.fromJson(response.data);
  }

}
