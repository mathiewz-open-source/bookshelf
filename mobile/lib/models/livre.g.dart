// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'livre.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Livre _$LivreFromJson(Map<String, dynamic> json) => Livre(
      id: json['id'] as String?,
      auteur: Auteur.fromJson(json['auteur'] as Map<String, dynamic>),
      titre: json['titre'] as String,
      edition: json['edition'] as String,
      prix: (json['prix'] as num).toDouble(),
      detenteurs: (json['detenteurs'] as List<dynamic>)
          .map((e) => User.fromJson(e as Map<String, dynamic>))
          .toList(),
      tags: (json['tags'] as List<dynamic>)
          .map((e) => Tag.fromJson(e as Map<String, dynamic>))
          .toList(),
      pret: json['pret'] as bool,
      lu: json['lu'] as bool,
    );

Map<String, dynamic> _$LivreToJson(Livre instance) => <String, dynamic>{
      'id': instance.id,
      'auteur': instance.auteur.toJson(),
      'titre': instance.titre,
      'edition': instance.edition,
      'prix': instance.prix,
      'detenteurs': instance.detenteurs.map((e) => e.toJson()).toList(),
      'tags': instance.tags.map((e) => e.toJson()).toList(),
      'pret': instance.pret,
      'lu': instance.lu,
    };
