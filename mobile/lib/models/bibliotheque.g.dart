// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bibliotheque.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Bibliotheque _$BibliothequeFromJson(Map<String, dynamic> json) => Bibliotheque(
      json['nom'] as String,
      json['nbLivres'] as int,
      (json['totalPrice'] as num).toDouble(),
      (json['livres'] as List<dynamic>)
          .map((e) => Livre.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BibliothequeToJson(Bibliotheque instance) =>
    <String, dynamic>{
      'nom': instance.nom,
      'nbLivres': instance.nbLivres,
      'totalPrice': instance.totalPrice,
      'livres': instance.livres.map((e) => e.toJson()).toList(),
    };
