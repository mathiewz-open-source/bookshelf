import 'package:json_annotation/json_annotation.dart';
import 'package:bookshelf/models/livre.dart';

part 'bibliotheque.g.dart';

@JsonSerializable(explicitToJson: true)
class Bibliotheque {

  late String nom;
  late int nbLivres;
  late double totalPrice;
  late List<Livre> livres;

  Bibliotheque(this.nom, this.nbLivres, this.totalPrice, this.livres);

  factory Bibliotheque.fromJson(Map<String, dynamic> json) => _$BibliothequeFromJson(json);
  Map<String, dynamic> toJson() => _$BibliothequeToJson(this);

}