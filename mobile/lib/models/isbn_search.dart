import 'package:json_annotation/json_annotation.dart';

import 'auteur.dart';

part 'isbn_search.g.dart';

@JsonSerializable(explicitToJson: true)
class IsbnSearch {

  late Auteur? auteur;
  late String? titre;
  late String? edition;
  late double? prix;

  IsbnSearch({this.auteur, this.titre, this.edition, this.prix});

  factory IsbnSearch.fromJson(Map<String, dynamic> json) => _$IsbnSearchFromJson(json);
  Map<String, dynamic> toJson() => _$IsbnSearchToJson(this);

}