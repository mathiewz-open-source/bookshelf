// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auteur.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Auteur _$AuteurFromJson(Map<String, dynamic> json) => Auteur(
      id: json['id'] as int?,
      nom: json['nom'] as String,
      prenom: json['prenom'] as String?,
    );

Map<String, dynamic> _$AuteurToJson(Auteur instance) => <String, dynamic>{
      'id': instance.id,
      'nom': instance.nom,
      'prenom': instance.prenom,
    };
