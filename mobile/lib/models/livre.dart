import 'package:json_annotation/json_annotation.dart';
import 'package:bookshelf/models/tag.dart';

import 'auteur.dart';
import 'user.dart';

part 'livre.g.dart';

@JsonSerializable(explicitToJson: true)
class Livre {

  late String? id;
  late Auteur auteur;
  late String titre;
  late String edition;
  late double prix;
  late List<User> detenteurs;
  late List<Tag> tags;
  late bool pret;
  late bool lu;

  Livre({this.id, required this.auteur, required this.titre, required this.edition, required this.prix, required this.detenteurs, required this.tags, required this.pret, required this.lu});

  factory Livre.fromJson(Map<String, dynamic> json) => _$LivreFromJson(json);
  Map<String, dynamic> toJson() => _$LivreToJson(this);

  String getOwners() {
    return detenteurs.map((user) => user.displayName).join(' & ');
  }

  bool ownedBy(User user) {
    return detenteurs.any((owner) => owner.userId == user.userId);
  }
}