import 'package:flutter/material.dart';

const defaultSpacing = EdgeInsets.all(16);
const defaultSpacingExceptBottom = EdgeInsets.only(top: 16, right: 16, left: 16);
