import 'package:flutter/material.dart';
import 'package:bookshelf/consts.dart';

class CardPadding extends StatelessWidget {
  final Widget child;
  final String? title;
  final Image? image;
  final double? titlePadding;

  const CardPadding({super.key, required this.child, this.image, this.title, this.titlePadding});

  @override
  Widget build(BuildContext context) {
    return Card(
        clipBehavior: image != null ? Clip.antiAliasWithSaveLayer : null,
        child: buildChild(context));
  }

  buildChild(BuildContext context) {
    List<Widget> children = [];

    if (image != null) {
      children.add(image!);
    }

    List<Widget> body = [];
    if (title != null) {
      var titleText = Padding(padding: EdgeInsets.only(bottom: titlePadding ?? 16), child: Text(title!, style: Theme.of(context).textTheme.headlineMedium));
      body.add(titleText);
    }
    body.add(child);

    children.add(Padding(
      padding: defaultSpacing,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: body,
      ),
    ));

    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: children);
  }
}
