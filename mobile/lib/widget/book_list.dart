import 'package:flutter/material.dart';
import 'package:bookshelf/models/livre.dart';
import 'package:bookshelf/pages/livre_view.dart';

class BookList extends StatelessWidget {

  final List<Livre> books;

  const BookList({super.key, required this.books});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: books.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(height: 1),
      itemBuilder: (context, index) {
        return bookTile(context, books[index]);
      },
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
    );
  }

  ListTile bookTile(BuildContext context, Livre livre) {
    return ListTile(
      title: Text(livre.titre),
      subtitle: Text(livre.auteur.getDisplayName()),
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => BookViewPage(livreId: livre.id!))),
      trailing: livre.lu ? const Icon(Icons.check_circle_outlined) : null,
    );
  }

}