import 'package:flutter/material.dart';

class TextDivider extends StatelessWidget {
  final String label;

  const TextDivider({super.key, required this.label});

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      const Expanded(child: Divider()),
      Text(label),
      const Expanded(child: Divider()),
    ]);
  }
}
