import 'package:dio/dio.dart';
import 'package:bookshelf/services/auth_service.dart';

class AuthInterceptor extends Interceptor {

  final authService = AuthService();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    var token = await authService.loadToken();
    if(token != null){
      options.headers.addAll({"Authorization": 'Bearer $token'});
    }
    super.onRequest(options, handler);
  }

}