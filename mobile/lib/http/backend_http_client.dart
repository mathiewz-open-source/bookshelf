import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:bookshelf/http/auth_interceptor.dart';

final backend = Dio();

void configureDio() {

  if(kDebugMode){
    backend.options.baseUrl = 'http://localhost:8080/';
  } else {
    backend.options.baseUrl = 'https://api.bookshelf-project.com/';
  }
  backend.options.connectTimeout = const Duration(seconds: 30);
  backend.options.receiveTimeout = const Duration(seconds: 30);
  backend.interceptors.add(AuthInterceptor());
}
