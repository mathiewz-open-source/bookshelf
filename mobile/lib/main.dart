import 'package:bookshelf/ThemeSettings.dart';
import 'package:bookshelf/http/backend_http_client.dart';
import 'package:bookshelf/pages/tab_navigation.dart';
import 'package:bookshelf/services/auth_service.dart';
import 'package:flutter/material.dart';

import 'pages/login.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureDio();
  ThemeSettings().loadPreferences();
  AuthService().loadToken().then((token) => runApp(Launcher(userAuthenticated: token != null)));
}

class Launcher extends StatefulWidget {

  final bool userAuthenticated;

  const Launcher({super.key, required this.userAuthenticated});

  @override
  State<StatefulWidget> createState() => LauncherState();
}

class LauncherState extends State<Launcher> {

  ThemeSettings themeSettings = ThemeSettings();

  @override
  Widget build(BuildContext context) {

    var landingPage = widget.userAuthenticated ? const TabNavigation() : const LoginForm();

    return ListenableBuilder(
        listenable: themeSettings,
        builder: (BuildContext context, Widget? child) {
          return MaterialApp(
              title: 'Bookshelf Project',
              theme: themeSettings.theme,
              darkTheme: themeSettings.darkTheme,
              themeMode: themeSettings.themeMode,
              home: landingPage);
        });
  }
}
